package edu.unl.nfs.nfsfire;

import android.app.ListActivity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.print.PrintAttributes;
import android.print.pdf.PrintedPdfDocument;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;


public class BillingActivity extends ListActivity implements View.OnClickListener {
    private ListView listView;

    private Button cancelButton;
    private Button sendEmailButton;

    public ArrayList <NFSBillingCellText> billingCellTexts;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_billing);

        initializeLabels();

        linkUiElements();
    }

    private void linkUiElements()
    {
        cancelButton = (Button) findViewById(R.id.button_cancel);
        sendEmailButton = (Button) findViewById(R.id.button_send_email);

        cancelButton.setOnClickListener(this);
        sendEmailButton.setOnClickListener(this);

        cancelButton.setBackgroundColor(Color.WHITE);
        sendEmailButton.setBackgroundColor(Color.WHITE);

        listView = (ListView) findViewById(android.R.id.list);

		NFSArrayAdapterBilling arrayAdapterBilling = new NFSArrayAdapterBilling(this, billingCellTexts);
		arrayAdapterBilling.billingActivity = this;

        setListAdapter(arrayAdapterBilling);
    }

    private void initializeLabels()
    {
        ArrayList <String> labels = new ArrayList <String> ();

        labels.add("Requesting Fire Department");
        labels.add("FD Officer Requesting Aerial Applicator");
        labels.add("Date and Time Aircraft Requested");
        labels.add("Date and Time Aircraft Dispatched");
        labels.add("Fire Location");
        labels.add("Approximate Acres Burned");
        labels.add("Total Gallons Retardant Used");
        labels.add("Aircraft Registration No.");
        labels.add("Aircraft Load");
        labels.add("Total Loads");
        labels.add("Total Hours Flying Time");
        labels.add("Rate Per Hour");
        labels.add("Total Amount");
        labels.add("Applicator Name");
        labels.add("Federal ID Number");
        labels.add("Social Security Number");
        labels.add("Address");
        labels.add("Phone");

        billingCellTexts = new ArrayList <NFSBillingCellText> ();

        for (String label : labels)
        {
            NFSBillingCellText cellText = new NFSBillingCellText();

            cellText.labelText = label;
            cellText.userText = "";

            billingCellTexts.add(cellText);
        }
    }

    private void generateBill()
    {
		PdfDocument document = new PdfDocument();
		PdfDocument.PageInfo pageInfo= new PdfDocument.PageInfo.Builder(612, 792, 1).create();
		PdfDocument.Page page = document.startPage(pageInfo);

		Paint paint = new Paint();

		InputStream inputStream = getResources().openRawResource(R.raw.form);
		Bitmap bitmap = BitmapFactory.decodeStream(inputStream);

		page.getCanvas().drawBitmap(bitmap, 0, 0, paint);

		// Requesting Fire Department
		page.getCanvas().drawText(billingCellTexts.get(0).userText, NFSConstants.REQ_FD_X, NFSConstants.REQ_FD_Y, paint);

		// FD Officer Requesting
		page.getCanvas().drawText(billingCellTexts.get(1).userText, NFSConstants.FD_OFFICER_X, NFSConstants.FD_OFFICER_Y, paint);

		// Date and time requested
		page.getCanvas().drawText(billingCellTexts.get(2).userText, NFSConstants.DT_REQ_X, NFSConstants.DT_REQ_Y, paint);

		// Date and time dispatched
		page.getCanvas().drawText(billingCellTexts.get(3).userText, NFSConstants.DT_DISP_X, NFSConstants.DT_DISP_Y, paint);

		// Fire Location
		page.getCanvas().drawText(billingCellTexts.get(4).userText, NFSConstants.FIRE_LOC_X, NFSConstants.FIRE_LOC_Y, paint);

		// Approximate Acres Burned
		page.getCanvas().drawText(billingCellTexts.get(5).userText, NFSConstants.ACRE_BURN_X, NFSConstants.ACRE_BURN_Y, paint);

		// Total Gallons Retardant Used
		page.getCanvas().drawText(billingCellTexts.get(6).userText, NFSConstants.GAL_RET_X, NFSConstants.GAL_RET_Y, paint);

		// Aircraft Registration 1
		page.getCanvas().drawText(billingCellTexts.get(7).userText, NFSConstants.REG_1_X, NFSConstants.REG_1_Y, paint);

		// Aircraft Load 1
		page.getCanvas().drawText(billingCellTexts.get(8).userText, NFSConstants.LOD_1_X, NFSConstants.LOD_1_Y, paint);

		// Total Loads 1
		page.getCanvas().drawText(billingCellTexts.get(9).userText, NFSConstants.TLD_1_X, NFSConstants.TLD_1_Y, paint);

		// Total Hours Flying Time 1
		page.getCanvas().drawText(billingCellTexts.get(10).userText, NFSConstants.HRS_1_X, NFSConstants.HRS_1_Y, paint);

		// Rate Per Hour 1
		page.getCanvas().drawText(billingCellTexts.get(11).userText, NFSConstants.RAT_1_X, NFSConstants.RAT_1_Y, paint);

		// Total Amount 1
		page.getCanvas().drawText(billingCellTexts.get(12).userText, NFSConstants.AMT_1_X, NFSConstants.AMT_1_Y, paint);

		// Applicator Name
		page.getCanvas().drawText(billingCellTexts.get(13).userText, NFSConstants.AP_NAME_X, NFSConstants.AP_NAME_Y, paint);

		// Federal ID Number
		page.getCanvas().drawText(billingCellTexts.get(14).userText, NFSConstants.FED_NUM_X, NFSConstants.FED_NUM_Y, paint);

		// Social Security Number
		page.getCanvas().drawText(billingCellTexts.get(15).userText, NFSConstants.SOC_X, NFSConstants.SOC_Y, paint);

		// Address
		page.getCanvas().drawText(billingCellTexts.get(16).userText, NFSConstants.ADDRESS_X, NFSConstants.ADDRESS_Y, paint);

		// Phone
		page.getCanvas().drawText(billingCellTexts.get(17).userText, NFSConstants.PHONE_X, NFSConstants.PHONE_Y, paint);

		document.finishPage(page);

		File file = new File(Environment.getExternalStorageDirectory(), "pdftests");
		if (!file.exists())
		{
			file.mkdirs();
		}

		File pdfFile = new File(file, "test.pdf");

		try
		{
			FileOutputStream fileOutputStream = new FileOutputStream(pdfFile);
			document.writeTo(fileOutputStream);
			document.close();
			fileOutputStream.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		Intent emailIntent = new Intent(Intent.ACTION_SEND);
		emailIntent.setType("vnd.android.cursor.dir/email");
		String to[] = {"nfsfire@unl.edu"};
		emailIntent.putExtra(Intent.EXTRA_EMAIL, to);
		emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(pdfFile));
		emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
		startActivity(Intent.createChooser(emailIntent, "Send Email"));
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.button_cancel:
                finish();
                break;

            case R.id.button_send_email:
                generateBill();
                break;
        }
    }
}
