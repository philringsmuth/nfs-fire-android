package edu.unl.nfs.nfsfire;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;


public class DetailActivity extends ListActivity
		implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, AdapterView.OnItemClickListener, View.OnClickListener, OnMapReadyCallback, GoogleMap.OnMarkerClickListener
{
	private Button listButton;
	private Button mapButton;
	private Button planesButton;
	private Button seatsButton;
	private Button foamsButton;

	private ListView listView;
    private MapView mapView;

	private ArrayList <NFSPlane> planes;
	private ArrayList <NFSSeat> seats;
	private ArrayList <NFSFoam> foams;

    private HashMap <Marker, NFSPlane> planeMarkers;
    private HashMap <Marker, NFSSeat> seatMarkers;
    private HashMap <Marker, NFSFoam> foamMarkers;

	Location currentLocation;

	boolean showPlanes;
	boolean showFoams;
	boolean showSeats;

	GoogleApiClient apiClient;
    GoogleMap googleMap;

    @Override
    protected void onCreate(Bundle savedInstanceState)
	{
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

		linkUiElements();

		initializePlanes();
		initializeSeats();
		initializeFoams();

		MapsInitializer.initialize(this);

        mapView.onCreate(savedInstanceState);
        buildGoogleApiClient();
        mapView.getMapAsync(this);
    }

	private void linkUiElements()
	{
		listButton = (Button) findViewById(R.id.button_list);
		mapButton = (Button) findViewById(R.id.button_map);
		planesButton = (Button) findViewById(R.id.button_planes);
		seatsButton = (Button) findViewById(R.id.button_seats);
		foamsButton = (Button) findViewById(R.id.button_foams);

		listButton.setOnClickListener(this);
		mapButton.setOnClickListener(this);
		planesButton.setOnClickListener(this);
		seatsButton.setOnClickListener(this);
		foamsButton.setOnClickListener(this);

        listButton.setBackgroundColor(Color.WHITE);
        mapButton.setBackgroundColor(Color.WHITE);
        planesButton.setBackgroundColor(Color.WHITE);
        seatsButton.setBackgroundColor(Color.WHITE);
        foamsButton.setBackgroundColor(Color.WHITE);

		listView = (ListView) findViewById(android.R.id.list);
        mapView = (MapView) findViewById(R.id.mapview);

		listView.setOnItemClickListener(this);
	}

	private void listButtonPressed()
	{
		listButton.setBackgroundColor(getResources().getColor(R.color.yellow));
        listView.setEnabled(true);
        listView.setAlpha(1.0f);

		mapButton.setBackgroundColor(Color.WHITE);
		mapView.setEnabled(false);
		mapView.setAlpha(0.0f);

		listView.bringToFront();
	}

	private void mapButtonPressed()
	{
        mapButton.setBackgroundColor(getResources().getColor(R.color.yellow));
        mapView.setEnabled(true);
        mapView.setAlpha(1.0f);

		listButton.setBackgroundColor(Color.WHITE);
        listView.setEnabled(false);
        listView.setAlpha(0.0f);

		mapView.bringToFront();

		plotMarkers();
	}

	private void planesButtonPressed()
	{
		selectDataButton(planesButton);
		sortPlanesByDistance();

		showPlanes = true;
		showSeats = false;
		showFoams = false;

        plotMarkers();

		setListAdapter(new NFSArrayAdapterPlanes(this, planes, currentLocation));
	}

	private void seatsButtonPressed()
	{
		selectDataButton(seatsButton);
		sortSeatsByDistance();

		showPlanes = false;
		showSeats = true;
		showFoams = false;

        plotMarkers();

		setListAdapter(new NFSArrayAdapterSeats(this, seats, currentLocation));
	}

	private void foamsButtonPressed()
	{
		selectDataButton(foamsButton);
		sortFoamsByDistance();

		showPlanes = false;
		showSeats = false;
		showFoams = true;

        plotMarkers();

		setListAdapter(new NFSArrayAdapterFoams(this, foams, currentLocation));
	}

	private void selectDataButton(Button button)
	{
		planesButton.setBackgroundColor(Color.WHITE);
		seatsButton.setBackgroundColor(Color.WHITE);
		foamsButton.setBackgroundColor(Color.WHITE);

		button.setBackgroundColor(getResources().getColor(R.color.yellow));
	}

	private void initializePlanes()
	{
		planes = (ArrayList <NFSPlane>) getIntent().getSerializableExtra(NFSConstants.EXTRA_PLANES);

		for (NFSPlane plane : planes)
		{
			plane.initializeLocation(this);
		}
	}

	private void initializeSeats()
	{
		seats = (ArrayList <NFSSeat>) getIntent().getSerializableExtra(NFSConstants.EXTRA_SEATS);

		for (NFSSeat seat : seats)
		{
			seat.initializeLocation(this);
		}
	}

	private void initializeFoams()
	{
		foams = (ArrayList <NFSFoam>) getIntent().getSerializableExtra(NFSConstants.EXTRA_FOAMS);

		for (NFSFoam foam : foams)
		{
			foam.initializeLocation(this);
		}
	}

	private void sortPlanesByDistance()
	{
		ArrayList <NFSPlane> unsortedPlanes = new ArrayList <NFSPlane> (planes);
		ArrayList <NFSPlane> sortedPlanes = new ArrayList <NFSPlane> ();

		while (unsortedPlanes.size() > 0)
		{
			int shortestDistance = Integer.MAX_VALUE;
			NFSPlane closestPlane = null;

			for (NFSPlane plane : unsortedPlanes)
			{
				int distance = (int) currentLocation.distanceTo(plane.coordinate);

				if (distance < shortestDistance)
				{
					shortestDistance = distance;
					closestPlane = plane;
				}
			}

			sortedPlanes.add(closestPlane);
			unsortedPlanes.remove(closestPlane);
		}

		planes = new ArrayList <NFSPlane> (sortedPlanes);
	}

	private void sortSeatsByDistance()
	{
		ArrayList <NFSSeat> unsortedSeats = new ArrayList <NFSSeat> (seats);
		ArrayList <NFSSeat> sortedSeats = new ArrayList <NFSSeat> ();

		while (unsortedSeats.size() > 0)
		{
			int shortestDistance = Integer.MAX_VALUE;
			NFSSeat closestSeat = null;

			for (NFSSeat seat : unsortedSeats)
			{
				int distance = (int) currentLocation.distanceTo(seat.coordinate);

				if (distance < shortestDistance)
				{
					shortestDistance = distance;
					closestSeat = seat;
				}
			}

			sortedSeats.add(closestSeat);
			unsortedSeats.remove(closestSeat);
		}

		seats = new ArrayList <NFSSeat> (sortedSeats);
	}

	private void sortFoamsByDistance()
	{
		ArrayList <NFSFoam> unsortedFoams = new ArrayList <NFSFoam> (foams);
		ArrayList <NFSFoam> sortedFoams = new ArrayList <NFSFoam> ();

		while (unsortedFoams.size() > 0)
		{
			int shortestDistance = Integer.MAX_VALUE;
			NFSFoam closestFoam = null;

			for (NFSFoam foam : unsortedFoams)
			{
				int distance = (int) currentLocation.distanceTo(foam.coordinate);

				if (distance < shortestDistance)
				{
					shortestDistance = distance;
					closestFoam = foam;
				}
			}

			sortedFoams.add(closestFoam);
			unsortedFoams.remove(closestFoam);
		}

		foams = new ArrayList <NFSFoam> (sortedFoams);
	}

	private void plotMarkers()
	{
		if (!mapView.isEnabled())
		{
			return;
		}

		if (showPlanes)
		{
			for (Marker planeMarker : planeMarkers.keySet())
			{
				planeMarker.setVisible(true);
			}

			for (Marker seatMarker : seatMarkers.keySet())
			{
				seatMarker.setVisible(false);
			}

			for (Marker foamMarker : foamMarkers.keySet())
			{
				foamMarker.setVisible(false);
			}
		}

		if (showSeats)
		{
			for (Marker planeMarker : planeMarkers.keySet())
			{
				planeMarker.setVisible(false);
			}

			for (Marker seatMarker : seatMarkers.keySet())
			{
				seatMarker.setVisible(true);
			}

			for (Marker foamMarker : foamMarkers.keySet())
			{
				foamMarker.setVisible(false);
			}
		}

		if (showFoams)
		{
			for (Marker planeMarker : planeMarkers.keySet())
			{
				planeMarker.setVisible(false);
			}

			for (Marker seatMarker : seatMarkers.keySet())
			{
				seatMarker.setVisible(false);
			}

			for (Marker foamMarker : foamMarkers.keySet())
			{
				foamMarker.setVisible(true);
			}
		}
	}

	private void showPlane(final NFSPlane plane)
	{
		String message = prepareMessageForPlane(plane);

		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		builder.setTitle(plane.businessName);
		builder.setMessage(message);
		builder.setPositiveButton("Call Day", new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				String phoneUri = "tel:" + plane.dayPhone.trim();
				Intent callIntent = new Intent(Intent.ACTION_CALL);
				callIntent.setData(Uri.parse(phoneUri));
				startActivity(callIntent);
			}
		});

		if (plane.alternatePhone.length() > 0)
		{
			builder.setNeutralButton("Call Alternate", new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					String phoneUri = "tel:" + plane.alternatePhone.trim();
					Intent callIntent = new Intent(Intent.ACTION_CALL);
					callIntent.setData(Uri.parse(phoneUri));
					startActivity(callIntent);
				}
			});
		}

		builder.show();
	}

	private void showSeat(NFSSeat seat)
	{
		String message = prepareMessageForSeat(seat);

		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		builder.setTitle(seat.county);
		builder.setMessage(message);
		builder.setPositiveButton("Cancel", null);

		builder.show();
	}

	private void showFoam(final NFSFoam foam)
	{
		String message = prepareMessageForFoam(foam);

		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		builder.setTitle(foam.location);
		builder.setMessage(message);
		builder.setPositiveButton("Call Day", new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				String phoneUri = "tel:" + foam.dayPhone.trim();
				Intent callIntent = new Intent(Intent.ACTION_CALL);
				callIntent.setData(Uri.parse(phoneUri));
				startActivity(callIntent);
			}
		});

		if (foam.nightPhone.length() > 0)
		{
			builder.setNeutralButton("Call Night", new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					String phoneUri = "tel:" + foam.nightPhone.trim();
					Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse(phoneUri));
                    startActivity(callIntent);
				}
			});
		}

		builder.show();
	}

	protected synchronized void buildGoogleApiClient()
	{
		apiClient = new GoogleApiClient.Builder(this)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.addApi(LocationServices.API)
				.build();
		apiClient.connect();
	}

	@Override
	public void onConnected(Bundle connectionHint)
	{
		if (currentLocation == null)
		{
			currentLocation = LocationServices.FusedLocationApi.getLastLocation(apiClient);

			listButtonPressed();
			planesButtonPressed();

			this.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), 8.0f));
		}
	}

	@Override
	public void onConnectionSuspended(int cause)
	{
		Log.d("DetailActivity", "Cause: " + cause);
	}

	@Override
	public void onConnectionFailed(ConnectionResult result)
	{
		Log.d("DetailActivity", "Connection Failed: " + result);
	}

	@Override
	public void onItemClick(AdapterView <?> parent, View view, int position, long id)
	{
		if (showPlanes)
		{
			showPlane(planes.get(position));
		}

		if (showSeats)
		{
			showSeat(seats.get(position));
		}

		if (showFoams)
		{
			showFoam(foams.get(position));
		}
	}

	@Override
	public void onClick(View v)
	{
		switch(v.getId())
		{
			case R.id.button_list:
				listButtonPressed();
				break;

			case R.id.button_map:
				mapButtonPressed();
				break;

			case R.id.button_planes:
				planesButtonPressed();
				break;

			case R.id.button_seats:
				seatsButtonPressed();
				break;

			case R.id.button_foams:
				foamsButtonPressed();
				break;
		}
	}

	private String prepareMessageForPlane(NFSPlane plane)
	{
		String message = new String();

		message = message + plane.ownerOperator + "\n";
		message = message + "Day Phone: " + plane.dayPhone + "\n";

		if (plane.alternatePhone.length() > 0)
		{
			message = message + "Alternate Phone: " + plane.alternatePhone + "\n";
		}

		message = message + "Email: " + plane.email + "\n";
		message = message + "Zip Code: " + plane.zipCode + "\n";

		message = message + "\n";

		message = message + "Number of Aircraft: " + plane.numberOfAircraft + "\n";
		message = message + "Class A Foam: " + plane.classAFoam + "\n";
		message = message + "Radio Frequency: " + plane.radioFrequency + "\n";

		message = message + "\n";

		message = message + tailTextString(plane.tailNumber1, plane.gallonCapacity1) + "\n";

		if (plane.tailNumber2.length() > 0)
		{
			message = message + tailTextString(plane.tailNumber2, plane.gallonCapacity2) + "\n";
		}

		if (plane.tailNumber3.length() > 0)
		{
			message = message + tailTextString(plane.tailNumber3, plane.gallonCapacity3) + "\n";
		}

		if (plane.tailNumber4.length() > 0)
		{
			message = message + tailTextString(plane.tailNumber4, plane.gallonCapacity4) + "\n";
		}

		if (plane.tailNumber5.length() > 0)
		{
			message = message + tailTextString(plane.tailNumber5, plane.gallonCapacity5) + "\n";
		}

		return message;
	}

	private String prepareMessageForSeat(NFSSeat seat)
	{
		String message = new String();

		message = message + seat.county + "\n";
		message = message + "Status: " + seat.status + "\n";

		return message;
	}

	private String prepareMessageForFoam(NFSFoam foam)
	{
		String message = new String();

		message = message + foam.location + "\n";
		message = message + "Contact: " + foam.contact + "\n";
		message = message + "Day Phone: " + foam.dayPhone + "\n";

		if (foam.nightPhone.length() > 0)
		{
			message = message + "Night Phone: " + foam.nightPhone + "\n";
		}

		message = message + "Zip Code: " + foam.zipCode + "\n";

		return message;
	}

	private String tailTextString(String tailNumber, String capacity)
	{
		return "Tail Number: " + tailNumber + ", " + capacity;
	}

    private void initializePlaneMarkers()
    {
        planeMarkers = new HashMap <Marker, NFSPlane> ();

        for (NFSPlane plane : planes)
        {
            LatLng latLng = new LatLng(plane.coordinate.getLatitude(), plane.coordinate.getLongitude());
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);

            Marker planeMarker = googleMap.addMarker(markerOptions);

			switch (Integer.valueOf(plane.numberOfAircraft))
			{
				case 1:
					planeMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.pin01));
					break;

				case 2:
					planeMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.pin02));
					break;

				case 3:
					planeMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.pin03));
					break;

				case 4:
					planeMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.pin04));
					break;

				case 5:
					planeMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.pin05));
					break;

				default:
					planeMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.pin));
					break;
			}

			planeMarker.setVisible(false);

            planeMarkers.put(planeMarker, plane);
        }
    }

    private void initializeSeatMarkers()
    {
        seatMarkers = new HashMap <Marker, NFSSeat> ();

        for (NFSSeat seat : seats)
        {
            LatLng latLng = new LatLng(seat.coordinate.getLatitude(), seat.coordinate.getLongitude());
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);

            Marker seatMarker = googleMap.addMarker(markerOptions);

			seatMarker.setVisible(true);
			seatMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.pin));

            seatMarkers.put(seatMarker, seat);
        }
    }

    private void initializeFoamMarkers()
    {
        foamMarkers = new HashMap <Marker, NFSFoam> ();

        for (NFSFoam foam : foams)
        {
            LatLng latLng = new LatLng(foam.coordinate.getLatitude(), foam.coordinate.getLongitude());
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);

            Marker foamMarker = googleMap.addMarker(markerOptions);

			foamMarker.setVisible(false);
			foamMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.pin));

            foamMarkers.put(foamMarker, foam);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        this.googleMap = googleMap;
		this.googleMap.setOnMarkerClickListener(this);
		this.googleMap.setMyLocationEnabled(true);

        initializePlaneMarkers();
        initializeSeatMarkers();
        initializeFoamMarkers();
    }

    @Override
    public void onDestroy()
    {
        mapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory()
    {
        mapView.onLowMemory();
        super.onLowMemory();
    }

    @Override
    public void onPause()
    {
        mapView.onPause();
        super.onPause();
    }

    @Override
    public void onResume()
    {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        mapView.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

	@Override
	public boolean onMarkerClick(Marker marker)
	{
		if (showPlanes)
		{
			NFSPlane plane = planeMarkers.get(marker);
			showPlane(plane);
		}

		if (showSeats)
		{
			NFSSeat seat = seatMarkers.get(marker);
			showSeat(seat);
		}

		if (showFoams)
		{
			NFSFoam foam = foamMarkers.get(marker);
			showFoam(foam);
		}

		return true;
	}
}
