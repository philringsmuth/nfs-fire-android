package edu.unl.nfs.nfsfire;

import android.location.Location;

/**
 * Created by philr on 4/8/15.
 */
public abstract class DirectionUtility
{
	public static double getHeadingForDirectionFromCoordinate(Location fromLocation, Location toLocation)
	{
		double fLat = degreesToRadians(fromLocation.getLatitude());
		double fLng = degreesToRadians(fromLocation.getLongitude());
		double tLat = degreesToRadians(toLocation.getLatitude());
		double tLng = degreesToRadians(toLocation.getLongitude());

		double degree = radiansToDegrees(Math.atan2(Math.sin(tLng-fLng)*Math.cos(tLat), Math.cos(fLat)*Math.sin(tLat)-Math.sin(fLat)*Math.cos(tLat)*Math.cos(tLng-fLng)));

		if (degree >= 0)
		{
			return degree;
		}
		else
		{
			return 360 + degree;
		}
	}

	public static String bearingToCompassDirection(double bearing)
	{
		if (bearing > NFSConstants.NORTH_NORTHWEST || bearing < NFSConstants.NORTH_NORTHEAST)
		{
			return NFSConstants.NORTH;
		}

		if (bearing > NFSConstants.NORTH_NORTHEAST && bearing < NFSConstants.EAST_NORTHEAST)
		{
			return NFSConstants.NORTHEAST;
		}

		if (bearing > NFSConstants.EAST_NORTHEAST && bearing < NFSConstants.EAST_SOUTHEAST)
		{
			return NFSConstants.EAST;
		}

		if (bearing > NFSConstants.EAST_SOUTHEAST && bearing < NFSConstants.SOUTH_SOUTHEAST)
		{
			return NFSConstants.SOUTHEAST;
		}

		if (bearing > NFSConstants.SOUTH_SOUTHEAST && bearing < NFSConstants.SOUTH_SOUTHWEST)
		{
			return NFSConstants.SOUTH;
		}

		if (bearing > NFSConstants.SOUTH_SOUTHWEST && bearing < NFSConstants.WEST_SOUTHWEST)
		{
			return NFSConstants.SOUTHWEST;
		}

		if (bearing > NFSConstants.WEST_SOUTHWEST && bearing < NFSConstants.WEST_NORTHWEST)
		{
			return NFSConstants.WEST;
		}

		if (bearing > NFSConstants.WEST_NORTHWEST && bearing < NFSConstants.NORTH_NORTHWEST)
		{
			return NFSConstants.NORTHWEST;
		}

		return "Unknown";
	}

	public static double degreesToRadians(double degrees)
	{
		return (Math.PI * degrees) / 180.0;
	}

	public static double radiansToDegrees(double radians)
	{
		return (radians * 180.0) / Math.PI;
	}
}