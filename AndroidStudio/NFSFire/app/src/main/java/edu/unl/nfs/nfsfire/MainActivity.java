package edu.unl.nfs.nfsfire;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;


public class MainActivity extends Activity implements View.OnClickListener
{
    private Button buttonFindAPlane;
    private Button buttonBillingForm;
    private Button buttonPlotAcreage;

	private ArrayList <NFSPlane> planes;
	private ArrayList <NFSSeat> seats;
	private ArrayList <NFSFoam> foams;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        linkUiElements();

		loadLocalData();
    }

    private void linkUiElements()
    {
        buttonFindAPlane = (Button) findViewById(R.id.button_find_a_plane);
        buttonBillingForm = (Button) findViewById(R.id.button_billing_form);
        buttonPlotAcreage = (Button) findViewById(R.id.button_plot_acreage);

        buttonFindAPlane.setOnClickListener(this);
        buttonBillingForm.setOnClickListener(this);
        buttonPlotAcreage.setOnClickListener(this);
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.button_find_a_plane:
                findPlaneButtonPressed();
                break;

            case R.id.button_billing_form:
                billingFormButtonPressed();
                break;

            case R.id.button_plot_acreage:
                plotAcreageButtonPressed();
                break;
        }
    }

    private void findPlaneButtonPressed()
    {
        Intent intent = new Intent(this, DetailActivity.class);

		intent.putExtra(NFSConstants.EXTRA_PLANES, planes);
		intent.putExtra(NFSConstants.EXTRA_SEATS, seats);
		intent.putExtra(NFSConstants.EXTRA_FOAMS, foams);

        startActivity(intent);
    }

    private void billingFormButtonPressed()
    {
        Intent intent = new Intent(this, BillingActivity.class);
        startActivity(intent);
    }

    private void plotAcreageButtonPressed()
    {
        Intent intent = new Intent(this, PlotAcreageActivity.class);
        startActivity(intent);
    }

	private void loadLocalData()
	{
		NFSDataSource dataSource = new NFSDataSource(this);

		planes = dataSource.getPlanes();
		seats = dataSource.getSeats();
		foams = dataSource.getFoams();
	}
}
