package edu.unl.nfs.nfsfire;

import android.os.Environment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by philringsmuth on 4/9/15.
 */
public abstract class NFSAreaFileManager
{
	public static void writeNewAreaToFile(String recentArea)
	{
		ArrayList <String> areasFromFile = readAreaFile();

		String areaStringTemp = new String();

		int i = 0;

		// Deletes the oldest entry, keeping the 10 most recent.
		// Can't use a for loop because initialization isn't always the same
		if (areasFromFile.size() > 9)
		{
			i = 1;
		}
		else
		{
			i = 0;
		}

		while (i < areasFromFile.size())
		{
			areaStringTemp = areaStringTemp + areasFromFile.get(i);
			areaStringTemp = areaStringTemp + "\n";
			i++;
		}

		areaStringTemp = areaStringTemp + recentArea;

		writeAreaFile(areaStringTemp);
	}

	public static ArrayList <String> readAreaFile()
	{
		ArrayList <String> results = new ArrayList <String> ();

		try
		{
			File root = new File(Environment.getExternalStorageDirectory(), NFSConstants.AREA_RESULTS_FOLDER);

			File areaFile = new File(root, NFSConstants.AREA_RESULTS_FILENAME);
			FileInputStream fileInputStream = new FileInputStream(areaFile);
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));

			String line = bufferedReader.readLine();

			while (line != null)
			{
				results.add(line);
				line = bufferedReader.readLine();
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		return results;
	}

	private static void writeAreaFile(String data)
	{
		try
		{
			File root = new File(Environment.getExternalStorageDirectory(), NFSConstants.AREA_RESULTS_FOLDER);
			if (!root.exists())
			{
				root.mkdirs();
			}

			File areaFile = new File(root, NFSConstants.AREA_RESULTS_FILENAME);
			FileWriter writer = new FileWriter(areaFile);
			writer.append(data);
			writer.flush();
			writer.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
