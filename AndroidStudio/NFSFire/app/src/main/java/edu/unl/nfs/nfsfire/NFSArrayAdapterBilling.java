package edu.unl.nfs.nfsfire;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by philringsmuth on 3/10/15.
 */
public class NFSArrayAdapterBilling
		extends ArrayAdapter <NFSBillingCellText>
{
	public BillingActivity billingActivity;

	private ArrayList <NFSBillingCellText> billingCellTexts;

	public NFSArrayAdapterBilling(Context context, ArrayList <NFSBillingCellText> billingCellTexts)
	{
		super(context, R.layout.detail_cell, billingCellTexts);
		this.billingCellTexts = billingCellTexts;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View cell = inflater.inflate(R.layout.billing_cell, parent, false);

        TextView labelText = (TextView) cell.findViewById(R.id.cellTitle);
        EditText editText = (EditText) cell.findViewById(R.id.edit_text);

        labelText.setText(billingCellTexts.get(position).labelText);
        editText.setText(billingCellTexts.get(position).userText);

		editText.addTextChangedListener(new TextWatcher()
		{
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after)
			{

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{

			}

			@Override
			public void afterTextChanged(Editable s)
			{
				billingActivity.billingCellTexts.get(position).userText = s.toString();
			}
		});

		return cell;
	}
}
