package edu.unl.nfs.nfsfire;

import android.content.Context;
import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by philringsmuth on 3/10/15.
 */
public class NFSArrayAdapterFoams
		extends ArrayAdapter <NFSFoam>
{
	private ArrayList <NFSFoam> foams;
	private Location location;

	public NFSArrayAdapterFoams(Context context, ArrayList <NFSFoam> foams, Location location)
	{
		super(context, R.layout.detail_cell, foams);
		this.foams = foams;
		this.location = location;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View cell = inflater.inflate(R.layout.detail_cell, parent, false);

		NFSFoam foam = foams.get(position);

		TextView line1 = (TextView) cell.findViewById(R.id.line1);
		TextView line2 = (TextView) cell.findViewById(R.id.line2);

		float distanceInMeters = location.distanceTo(foam.coordinate);
		int distanceInMiles = (int) (distanceInMeters / NFSConstants.METERS_PER_MILE);

		double degrees = DirectionUtility.getHeadingForDirectionFromCoordinate(location, foam.coordinate);
		String direction = DirectionUtility.bearingToCompassDirection(degrees);

		line1.setText(foam.location);
		line2.setText(distanceInMiles + " miles " + direction);

		return cell;
	}
}
