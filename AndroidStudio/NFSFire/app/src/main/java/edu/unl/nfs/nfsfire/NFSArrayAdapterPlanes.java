package edu.unl.nfs.nfsfire;

import android.content.Context;
import android.location.Location;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by philringsmuth on 3/10/15.
 */
public class NFSArrayAdapterPlanes extends ArrayAdapter <NFSPlane>
{
	private ArrayList<NFSPlane> planes;
	private Location location;

	public NFSArrayAdapterPlanes(Context context, ArrayList<NFSPlane> planes, Location location)
	{
		super(context, R.layout.detail_cell, planes);
		this.planes = planes;
		this.location = location;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View cell = inflater.inflate(R.layout.detail_cell, parent, false);

		NFSPlane plane = planes.get(position);

		TextView line1 = (TextView) cell.findViewById(R.id.line1);
		TextView line2 = (TextView) cell.findViewById(R.id.line2);

		float distanceInMeters = location.distanceTo(plane.coordinate);
		int distanceInMiles = (int) (distanceInMeters / NFSConstants.METERS_PER_MILE);

		double degrees = DirectionUtility.getHeadingForDirectionFromCoordinate(location, plane.coordinate);
		String direction = DirectionUtility.bearingToCompassDirection(degrees);

		line1.setText(plane.businessName);
		line2.setText(distanceInMiles + " miles " + direction);

		return cell;
	}

}
