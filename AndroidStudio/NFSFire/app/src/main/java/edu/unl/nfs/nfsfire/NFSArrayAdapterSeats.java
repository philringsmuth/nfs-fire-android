package edu.unl.nfs.nfsfire;

import android.content.Context;
import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by philringsmuth on 3/10/15.
 */
public class NFSArrayAdapterSeats
		extends ArrayAdapter <NFSSeat>
{
	private ArrayList <NFSSeat> seats;
	private Location location;

	public NFSArrayAdapterSeats(Context context, ArrayList <NFSSeat> seats, Location location)
	{
		super(context, R.layout.detail_cell, seats);
		this.seats = seats;
		this.location = location;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View cell = inflater.inflate(R.layout.detail_cell, parent, false);

		NFSSeat seat = seats.get(position);

		TextView line1 = (TextView) cell.findViewById(R.id.line1);
		TextView line2 = (TextView) cell.findViewById(R.id.line2);

		float distanceInMeters = location.distanceTo(seat.coordinate);
		int distanceInMiles = (int) (distanceInMeters / NFSConstants.METERS_PER_MILE);

		double degrees = DirectionUtility.getHeadingForDirectionFromCoordinate(location, seat.coordinate);
		String direction = DirectionUtility.bearingToCompassDirection(degrees);

		line1.setText(seat.county);
		line2.setText(distanceInMiles + " miles " + direction);

		return cell;
	}
}
