package edu.unl.nfs.nfsfire;

/**
 * Created by philringsmuth on 3/10/15.
 */
public class NFSConstants
{
	public static final String HTTP_METHOD_GET = "GET";

	public static final String PLANES_TABLE_ID = "1E9jJAVYB2AMTrYQ3WYKsfZ_0kMpSAYAllsf9_tVs";
	public static final String FOAMS_TABLE_ID = "1qvZgWP5kTTyJZCvwv5WnLiIAkNX8M3g-XWwIdF3u";
	public static final String SEATS_TABLE_ID = "1xY8P68LCcKt0d6mgUVNlJO07buQo20QVTVVVYoQ6";

	public static final String API_KEY = "AIzaSyAxP0sbFJ2FX6CJOojTYADPg74h-5jvNh0";

	public static final String TABLE_ADDRESS_PREFIX = "https://www.googleapis.com/fusiontables/v1/query?sql=SELECT%20*%20FROM%20";

	public static final String COLUMNS = "columns";
	public static final String ROWS = "rows";

	public static final String ZIPCODES_FILE_NAME = "zipcodes";
	public static final String CSV_EXTENSION = "csv";
	public static final String NEW_LINE = "\n";
	public static final String COMMA = ",";
	public static final String HYPHEN = "-";

	public static final String EXTRA_PLANES = "planes";
	public static final String EXTRA_SEATS = "seats";
	public static final String EXTRA_FOAMS = "foams";



	public static final String NORTH = "North";
	public static final String NORTHEAST = "Northeast";
	public static final String EAST = "East";
	public static final String SOUTHEAST = "Southeast";
	public static final String SOUTH = "South";
	public static final String SOUTHWEST = "Southwest";
	public static final String WEST = "West";
	public static final String NORTHWEST = "Northwest";

	public static final String PLANES_LOCAL_FILENAME = "planes_local.json";
	public static final String SEATS_LOCAL_FILENAME = "seats_local.json";
	public static final String FOAMS_LOCAL_FILENAME = "foams_local.json";
	public static final String AREA_RESULTS_FILENAME = "recentAreaResults.txt";
	public static final String AREA_RESULTS_FOLDER = "NFS_Area_Results";

	public static final int TIMEOUT_INTERVAL = 10;

	public static final double NORTH_NORTHEAST = 22.5;
	public static final double EAST_NORTHEAST = 67.5;
	public static final double EAST_SOUTHEAST = 112.5;
	public static final double SOUTH_SOUTHEAST = 157.5;
	public static final double SOUTH_SOUTHWEST = 202.5;
	public static final double WEST_SOUTHWEST = 247.5;
	public static final double WEST_NORTHWEST = 292.5;
	public static final double NORTH_NORTHWEST = 337.5;

	public static final double FADE_DURATION = 0.25;

	public static final double LATITUDE_DELTA = 1.0;
	public static final double LONGITUDE_DELTA = 1.0;

	public static final double METERS_PER_MILE = 1609.34;

	public static double degreesToRadians(double x)
	{
		return (Math.PI * x) / 180.0;
	}

	public static final double radiansToDegrees(double x)
	{
		return (x * 180.0) / Math.PI;
	}

	public static final int REQ_FD_X			= 25;
	public static final int REQ_FD_Y			= 73;
	public static final int FD_OFFICER_X		= 295;
	public static final int FD_OFFICER_Y		= 73;
	public static final int DT_REQ_X			= 117;
	public static final int DT_REQ_Y			= 95;
	public static final int DT_DISP_X			= 389;
	public static final int DT_DISP_Y			= 95;
	public static final int FIRE_LOC_X			= 25;
	public static final int FIRE_LOC_Y			= 131;
	public static final int ACRE_BURN_X			= 152;
	public static final int ACRE_BURN_Y			= 201;
	public static final int GAL_RET_X			= 440;
	public static final int GAL_RET_Y			= 201;

	public static final int REG_1_X				= 25;
	public static final int REG_1_Y				= 260;
	public static final int REG_2_X				= 25;
	public static final int REG_2_Y				= 277;
	public static final int REG_3_X				= 25;
	public static final int REG_3_Y				= 293;
	public static final int REG_4_X				= 25;
	public static final int REG_4_Y				= 311;
	public static final int REG_5_X				= 25;
	public static final int REG_5_Y				= 329;
	public static final int REG_6_X				= 25;
	public static final int REG_6_Y				= 346;
	public static final int REG_7_X				= 25;
	public static final int REG_7_Y				= 364;

	public static final int LOD_1_X				= 150;
	public static final int LOD_1_Y				= 260;
	public static final int LOD_2_X				= 150;
	public static final int LOD_2_Y				= 277;
	public static final int LOD_3_X				= 150;
	public static final int LOD_3_Y				= 293;
	public static final int LOD_4_X				= 150;
	public static final int LOD_4_Y				= 311;
	public static final int LOD_5_X				= 150;
	public static final int LOD_5_Y				= 329;
	public static final int LOD_6_X				= 150;
	public static final int LOD_6_Y				= 346;
	public static final int LOD_7_X				= 150;
	public static final int LOD_7_Y				= 364;

	public static final int TLD_1_X				= 225;
	public static final int TLD_1_Y				= 260;
	public static final int TLD_2_X				= 225;
	public static final int TLD_2_Y				= 277;
	public static final int TLD_3_X				= 225;
	public static final int TLD_3_Y				= 293;
	public static final int TLD_4_X				= 225;
	public static final int TLD_4_Y				= 311;
	public static final int TLD_5_X				= 225;
	public static final int TLD_5_Y				= 329;
	public static final int TLD_6_X				= 225;
	public static final int TLD_6_Y				= 346;
	public static final int TLD_7_X				= 225;
	public static final int TLD_7_Y				= 364;

	public static final int HRS_1_X				= 300;
	public static final int HRS_1_Y				= 260;
	public static final int HRS_2_X				= 300;
	public static final int HRS_2_Y				= 277;
	public static final int HRS_3_X				= 300;
	public static final int HRS_3_Y				= 293;
	public static final int HRS_4_X				= 300;
	public static final int HRS_4_Y				= 311;
	public static final int HRS_5_X				= 300;
	public static final int HRS_5_Y				= 329;
	public static final int HRS_6_X				= 300;
	public static final int HRS_6_Y				= 346;
	public static final int HRS_7_X				= 300;
	public static final int HRS_7_Y				= 364;

	public static final int RAT_1_X				= 420;
	public static final int RAT_1_Y				= 260;
	public static final int RAT_2_X				= 420;
	public static final int RAT_2_Y				= 277;
	public static final int RAT_3_X				= 420;
	public static final int RAT_3_Y				= 293;
	public static final int RAT_4_X				= 420;
	public static final int RAT_4_Y				= 311;
	public static final int RAT_5_X				= 420;
	public static final int RAT_5_Y				= 329;
	public static final int RAT_6_X				= 420;
	public static final int RAT_6_Y				= 346;
	public static final int RAT_7_X				= 420;
	public static final int RAT_7_Y				= 364;

	public static final int AMT_1_X				= 490;
	public static final int AMT_1_Y				= 260;
	public static final int AMT_2_X				= 490;
	public static final int AMT_2_Y				= 277;
	public static final int AMT_3_X				= 490;
	public static final int AMT_3_Y				= 293;
	public static final int AMT_4_X				= 490;
	public static final int AMT_4_Y				= 311;
	public static final int AMT_5_X				= 490;
	public static final int AMT_5_Y				= 329;
	public static final int AMT_6_X				= 490;
	public static final int AMT_6_Y				= 346;
	public static final int AMT_7_X				= 490;
	public static final int AMT_7_Y				= 364;

	public static final int HOURS_BILLED_X		= 300;
	public static final int HOURS_BILLED_Y		= 383;
	public static final int AMOUNT_BILLED_X		= 495;
	public static final int AMOUNT_BILLED_Y		= 383;
	public static final int AP_NAME_X			= 110;
	public static final int AP_NAME_Y			= 395;
	public static final int FED_NUM_X			= 120;
	public static final int FED_NUM_Y			= 417;
	public static final int SOC_X				= 435;
	public static final int SOC_Y				= 417;
	public static final int ADDRESS_X			= 25;
	public static final int ADDRESS_Y			= 450;
	public static final int PHONE_X				= 25;
	public static final int PHONE_Y				= 510;
	public static final int SIG_X				= 260;
	public static final int SIG_Y				= 510;
	public static final int DAT_SUB_X			= 100;
	public static final int DAT_SUB_Y			= 550;
	public static final int EMAIL_X				= 295;
	public static final int EMAIL_Y				= 550;
}

