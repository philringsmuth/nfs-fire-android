package edu.unl.nfs.nfsfire;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by philringsmuth on 3/15/15.
 */
public class NFSDataSource
{
	private Context context;

	private ArrayList <NFSPlane> planes;
	private ArrayList <NFSFoam>  foams;
	private ArrayList <NFSSeat>  seats;

	public NFSDataSource(Context context)
	{
		this.context = context;

		loadPlanesDataFromLocal();
		loadFoamsDataFromLocal();
		loadSeatsDataFromLocal();

		new FetchRemoteDataTask().execute();
	}

	public ArrayList <NFSPlane> getPlanes()
	{
		return planes;
	}

	public ArrayList <NFSFoam> getFoams()
	{
		return foams;
	}

	public ArrayList <NFSSeat> getSeats()
	{
		return seats;
	}

	private void loadPlanesDataFromRemote()
	{
		String planesString = loadRemoteDataFromTable(NFSConstants.PLANES_TABLE_ID);

		if (planesString != null)
		{
			saveLocalCopy(planesString, NFSConstants.PLANES_LOCAL_FILENAME);
		}
	}

	private void loadFoamsDataFromRemote()
	{
		String foamsString = loadRemoteDataFromTable(NFSConstants.FOAMS_TABLE_ID);

		if (foamsString != null)
		{
			saveLocalCopy(foamsString, NFSConstants.FOAMS_LOCAL_FILENAME);
		}
	}

	private void loadSeatsDataFromRemote()
	{
		String seatsString = loadRemoteDataFromTable(NFSConstants.SEATS_TABLE_ID);

		if (seatsString != null)
		{
			saveLocalCopy(seatsString, NFSConstants.SEATS_LOCAL_FILENAME);
		}
	}

	private void loadPlanesDataFromLocal()
	{
		String planesString = loadLocalDataFromFile(NFSConstants.PLANES_LOCAL_FILENAME);

		ArrayList <Object> results = loadDataFromString(planesString);
		planes = new ArrayList <NFSPlane> ();

		for (Object object : results)
		{
			NFSPlane plane = (NFSPlane) object;

			// Only Load planes listed as Active
			if (plane.status.equalsIgnoreCase("Active"))
			{
				planes.add(plane);
			}
		}
	}

	private void loadFoamsDataFromLocal()
	{
		String foamsString = loadLocalDataFromFile(NFSConstants.FOAMS_LOCAL_FILENAME);

		ArrayList <Object> results = loadDataFromString(foamsString);
		foams = new ArrayList <NFSFoam> ();

		for (Object foam : results)
		{
			foams.add((NFSFoam) foam);
		}
	}

	private void loadSeatsDataFromLocal()
	{
		String seatsString = loadLocalDataFromFile(NFSConstants.SEATS_LOCAL_FILENAME);

		ArrayList <Object> results = loadDataFromString(seatsString);
		seats = new ArrayList <NFSSeat> ();

		for (Object seat : results)
		{
			seats.add((NFSSeat) seat);
		}
	}

	private String urlStringForTableId(String tableId)
	{
		return NFSConstants.TABLE_ADDRESS_PREFIX + tableId + "&key=" + NFSConstants.API_KEY;
	}

	private void saveLocalCopy(String jsonString, String fileName)
	{
		try
		{
			File root = new File(Environment.getExternalStorageDirectory(), "NFS_Fire");

			if (!root.exists())
			{
				root.mkdirs();
			}

			File localCopy = new File(root, fileName);
			FileWriter writer = new FileWriter(localCopy);
			writer.append(jsonString);
			writer.flush();
			writer.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private String loadLocalDataFromFile(String fileName)
	{
		File root;
		InputStream inputStream = null;
		String content = new String();
		boolean loadFromLocalCopy = false;

		try
		{
			root = new File(Environment.getExternalStorageDirectory(), "NFS_Fire");

			if (root.exists())
			{
				File localCopy = new File(root, fileName);
				if (localCopy.exists())
				{
					loadFromLocalCopy = true;
				}
			}

			if (loadFromLocalCopy)
			{
				inputStream = new FileInputStream(new File(root, fileName));
			}
			else
			{
				if (fileName.equalsIgnoreCase(NFSConstants.PLANES_LOCAL_FILENAME))
				{
					inputStream = context.getResources().openRawResource(R.raw.planes_local);
				}

				if (fileName.equalsIgnoreCase(NFSConstants.SEATS_LOCAL_FILENAME))
				{
					inputStream = context.getResources().openRawResource(R.raw.seats_local);
				}

				if (fileName.equalsIgnoreCase(NFSConstants.FOAMS_LOCAL_FILENAME))
				{
					inputStream = context.getResources().openRawResource(R.raw.foams_local);
				}
			}

			if (inputStream != null)
			{
				InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
				BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

				String received = new String();
				StringBuilder stringBuilder = new StringBuilder();

				while ( (received = bufferedReader.readLine()) != null )
				{
					stringBuilder.append(received);
				}

				inputStream.close();
				content = stringBuilder.toString();
			}
		}
		catch (NullPointerException e)
		{
			e.printStackTrace();
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		return content;
	}

	private String loadRemoteDataFromTable(String tableId)
	{
		String content = new String();

		try
		{
			URL url = new URL(urlStringForTableId(tableId));
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

			InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
			InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
			BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

			String received = new String();
			StringBuilder stringBuilder = new StringBuilder();

			while ( (received = bufferedReader.readLine()) != null )
			{
				stringBuilder.append(received);
			}

			inputStream.close();
			content = stringBuilder.toString();
		}
		catch (MalformedURLException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		return content;
	}

	private ArrayList <Object> loadDataFromString(String jsonString)
	{
		ArrayList <Object> results = new ArrayList <Object> ();

		try
		{
			JSONObject jsonObject = new JSONObject(jsonString);

			JSONArray columns = jsonObject.getJSONArray(NFSConstants.COLUMNS);
			JSONArray rows = jsonObject.getJSONArray(NFSConstants.ROWS);

			int columnCount = columns.length();
			int rowCount = rows.length();

			for (int row = 0; row < rowCount; row++)
			{
				JSONArray rowValues = rows.getJSONArray(row);

				ArrayList <String> values = new ArrayList <String> ();

				for (int i = 0; i < columnCount; i++)
				{
					String columnValue = rowValues.getString(i);
					values.add(columnValue);
				}

				// This should probably be done better, store these numbers in NFSConstants maybe?
				if (columnCount == 24)
				{
					NFSPlane plane = new NFSPlane(values);
					results.add(plane);
				}
				else if (columnCount == 7)
				{
					NFSFoam foam = new NFSFoam(values, context);
					results.add(foam);
				}
				else if (columnCount == 4)
				{
					NFSSeat seat = new NFSSeat(values, context);
					results.add(seat);
				}
			}
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}

		return results;
	}

	private class FetchRemoteDataTask extends AsyncTask <Void, Void, Void>
	{
		protected Void doInBackground(Void... params)
		{
			loadPlanesDataFromRemote();
			loadFoamsDataFromRemote();
			loadSeatsDataFromRemote();

			return null;
		}
	}
}