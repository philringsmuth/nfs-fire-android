package edu.unl.nfs.nfsfire;

import android.content.Context;
import android.location.Location;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by philringsmuth on 3/11/15.
 */
public class NFSFoam implements Serializable
{
	public String location;
	public String contact;
	public String dayPhone;
	public String nightPhone;
	public String zipCode;
	public String latitudeCoordinates;
	public String longitudeCoordinates;

	public Location coordinate;

	public NFSFoam(ArrayList<String> values, Context context)
	{
		setPropertiesWithValues(values);
	}

	private void setPropertiesWithValues(ArrayList <String> values)
	{
		location				= values.get(0);
		contact					= values.get(1);
		dayPhone				= values.get(2);
		nightPhone				= values.get(3);
		zipCode					= values.get(4);
		latitudeCoordinates		= values.get(5);
		longitudeCoordinates	= values.get(6);

		zipCode = zipCode.split(NFSConstants.HYPHEN)[0];
	}

	public void initializeLocation(Context context)
	{
		double latitudeDegrees = 0.0;
		double longitudeDegrees = 0.0;

		if (latitudeCoordinates.length() > 0 && longitudeCoordinates.length() > 0)
		{
			// Separate the coordinates into components by removing whitespace separators
			List <String> latitudeComponents = Arrays.asList(latitudeCoordinates.split("\\s+"));
			List <String> longitudeComponents = Arrays.asList(longitudeCoordinates.split("\\s+"));

			// This happens if the coordinates are in the form of a single decimal value
			if (latitudeComponents.size() == 1 && longitudeComponents.size() == 1)
			{
				latitudeDegrees = Double.parseDouble(latitudeComponents.get(0));
				longitudeDegrees = Double.parseDouble(longitudeComponents.get(0));
			}

			// This happens if the coordinates are separated into DD MM SS format.
			else
			{
				// Remove any non-numeric/decimal characters from each component
				ArrayList <String> mutableLatitudeComponents = new ArrayList <String> ();
				ArrayList <String> mutableLongitudeComponents = new ArrayList <String> ();

				for (String component : latitudeComponents)
				{
					component = component.replaceAll("[^\\d.]", "");
					mutableLatitudeComponents.add(component);
				}

				for (String component : longitudeComponents)
				{
					component = component.replaceAll("[^\\d.]", "");
					mutableLongitudeComponents.add(component);
				}

				while (mutableLatitudeComponents.size() < 3)
				{
					mutableLatitudeComponents.add("0");
				}

				while (mutableLongitudeComponents.size() < 3)
				{
					mutableLongitudeComponents.add("0");
				}

				latitudeDegrees = Double.parseDouble(mutableLatitudeComponents.get(0));
				double latitudeMinutes = Double.parseDouble(mutableLatitudeComponents.get(1));
				double latitudeSeconds = Double.parseDouble(mutableLatitudeComponents.get(2));

				latitudeMinutes = latitudeMinutes + (latitudeSeconds / 60);
				latitudeDegrees = latitudeDegrees + (latitudeMinutes / 60);

				longitudeDegrees = Double.parseDouble(mutableLongitudeComponents.get(0));
				double longitudeMinutes = Double.parseDouble(mutableLongitudeComponents.get(1));
				double longitudeSeconds = Double.parseDouble(mutableLongitudeComponents.get(2));

				longitudeMinutes = longitudeMinutes + (longitudeSeconds / 60);
				longitudeDegrees = longitudeDegrees + (longitudeMinutes / 60);

				if (longitudeDegrees > 0)
				{
					longitudeDegrees = -longitudeDegrees;
				}
			}

			coordinate = new Location("");
			coordinate.setLatitude(latitudeDegrees);
			coordinate.setLongitude(longitudeDegrees);
		}

		else
		{
			InputStream inputStream = context.getResources().openRawResource(R.raw.zipcodes);

			ArrayList <String> lines = new ArrayList <String> ();

			try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream)))
			{
				String line;

				while ((line = bufferedReader.readLine()) != null)
				{
					lines.add(line);
				}

				bufferedReader.close();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}

			for (String line : lines)
			{
				List <String> components = Arrays.asList(line.split(NFSConstants.COMMA));

				if (components.get(0).equalsIgnoreCase(zipCode))
				{
					String latitudeString = components.get(1);
					String longitudeString = components.get(2);

					double latitude = Double.valueOf(latitudeString);
					double longitude = Double.valueOf(longitudeString);

					coordinate = new Location("");
					coordinate.setLatitude(latitude);
					coordinate.setLongitude(longitude);
				}
			}
		}
	}
}
