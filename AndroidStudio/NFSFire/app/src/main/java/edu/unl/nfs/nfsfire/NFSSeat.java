package edu.unl.nfs.nfsfire;

import android.content.Context;
import android.location.Location;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by philringsmuth on 3/11/15.
 */
public class NFSSeat implements Serializable
{
	public String county;
	public String latitudeCoordinates;
	public String longitudeCoordinates;
	public String status;

	public Location coordinate;

	public NFSSeat(ArrayList<String> values, Context context)
	{
		setPropertiesWithValues(values);
	}

	private void setPropertiesWithValues(ArrayList <String> values)
	{
		county					= values.get(0);
		status					= values.get(1);
		latitudeCoordinates		= values.get(2);
		longitudeCoordinates	= values.get(3);
	}

	public void initializeLocation(Context context)
	{
		double latitudeDegrees = 0.0;
		double longitudeDegrees = 0.0;

		if (latitudeCoordinates.length() > 0 && longitudeCoordinates.length() > 0)
		{
			// Separate the coordinates into components by removing whitespace separators
			List <String> latitudeComponents = Arrays.asList(latitudeCoordinates.split("\\s+"));
			List <String> longitudeComponents = Arrays.asList(longitudeCoordinates.split("\\s+"));

			// This happens if the coordinates are in the form of a single decimal value
			if (latitudeComponents.size() == 1 && longitudeComponents.size() == 1)
			{
				latitudeDegrees = Double.parseDouble(latitudeComponents.get(0));
				longitudeDegrees = Double.parseDouble(longitudeComponents.get(0));
			}

			// This happens if the coordinates are separated into DD MM SS format.
			else
			{
				// Remove any non-numeric/decimal characters from each component
				ArrayList <String> mutableLatitudeComponents = new ArrayList <String> ();
				ArrayList <String> mutableLongitudeComponents = new ArrayList <String> ();

				for (String component : latitudeComponents)
				{
					component = component.replaceAll("[^\\d.]", "");
					mutableLatitudeComponents.add(component);
				}

				for (String component : longitudeComponents)
				{
					component = component.replaceAll("[^\\d.]", "");
					mutableLongitudeComponents.add(component);
				}

				while (mutableLatitudeComponents.size() < 3)
				{
					mutableLatitudeComponents.add("0");
				}

				while (mutableLongitudeComponents.size() < 3)
				{
					mutableLongitudeComponents.add("0");
				}

				latitudeDegrees = Double.parseDouble(mutableLatitudeComponents.get(0));
				double latitudeMinutes = Double.parseDouble(mutableLatitudeComponents.get(1));
				double latitudeSeconds = Double.parseDouble(mutableLatitudeComponents.get(2));

				latitudeMinutes = latitudeMinutes + (latitudeSeconds / 60);
				latitudeDegrees = latitudeDegrees + (latitudeMinutes / 60);

				longitudeDegrees = Double.parseDouble(mutableLongitudeComponents.get(0));
				double longitudeMinutes = Double.parseDouble(mutableLongitudeComponents.get(1));
				double longitudeSeconds = Double.parseDouble(mutableLongitudeComponents.get(2));

				longitudeMinutes = longitudeMinutes + (longitudeSeconds / 60);
				longitudeDegrees = longitudeDegrees + (longitudeMinutes / 60);

				if (longitudeDegrees > 0)
				{
					longitudeDegrees = -longitudeDegrees;
				}
			}

			coordinate = new Location("");
			coordinate.setLatitude(latitudeDegrees);
			coordinate.setLongitude(longitudeDegrees);
		}
	}
}
