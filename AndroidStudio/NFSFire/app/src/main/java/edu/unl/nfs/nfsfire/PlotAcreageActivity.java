package edu.unl.nfs.nfsfire;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Color;
import android.location.Location;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class PlotAcreageActivity extends Activity implements View.OnClickListener, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private Button buttonStart;
    private Button buttonPause;
    private Button buttonViewRecents;
    private MapView mapView;

    private boolean plotting;
    private boolean paused;

    ArrayList <Location> storedLocations;
	ArrayList <Polyline> polylines;

    LocationRequest locationRequest;

    GoogleApiClient apiClient;
    GoogleMap googleMap;

	private static final int MIN_DISTANCE = 25;
	private static final int MIN_POINTS_FOR_LOOP = 3;
	private static final int ACRES_PER_SQUARE_DEGREE = 2356421;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plot_acreage);

		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

	    storedLocations = new ArrayList <Location> ();
	    polylines = new ArrayList <Polyline> ();

        linkUiElements();

        plotting = false;
        paused = false;

        buttonPause.setEnabled(false);
        buttonPause.setTextColor(Color.LTGRAY);

        MapsInitializer.initialize(this);

        mapView.onCreate(savedInstanceState);
        buildGoogleApiClient();
        mapView.getMapAsync(this);

        createLocationRequest();
    }

    private void linkUiElements()
    {
        buttonStart = (Button) findViewById(R.id.button_start);
        buttonPause = (Button) findViewById(R.id.button_pause);
        buttonViewRecents = (Button) findViewById(R.id.button_view_recent);

        buttonStart.setOnClickListener(this);
        buttonPause.setOnClickListener(this);
        buttonViewRecents.setOnClickListener(this);

	    mapView = (MapView) findViewById(R.id.mapview);

	    buttonStart.setBackgroundColor(Color.WHITE);
	    buttonPause.setBackgroundColor(Color.WHITE);
	    buttonViewRecents.setBackgroundColor(Color.WHITE);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.button_start:
                startButtonPressed();
                break;

            case R.id.button_pause:
                pauseButtonPressed();
                break;

            case R.id.button_view_recent:
                viewRecentsButtonPressed();
                break;
        }
    }

    private void startButtonPressed()
    {
        if (!plotting)
        {
            // Remove overlays from previous runs
	        for (Polyline polyline : polylines)
	        {
		        polyline.remove();
	        }

            buttonStart.setText("Stop and Save");
            buttonPause.setEnabled(true);
            buttonPause.setTextColor(Color.BLACK);

            storedLocations = new ArrayList <Location> ();
	        polylines = new ArrayList <Polyline> ();

	        startLocationUpdates();

            plotting = true;
        }
        else
        {
	        // Stop and save
            stopLocationUpdates();

	        // Finish the loop
            if (storedLocations.size() > 0)
            {
                Location firstPoint = storedLocations.get(0);
                Location lastPoint = storedLocations.get(storedLocations.size() - 1);

	            LatLng latLng1 = new LatLng(firstPoint.getLatitude(), firstPoint.getLongitude());
	            LatLng latlng2 = new LatLng(lastPoint.getLatitude(), lastPoint.getLongitude());

                // draw line on map
	            PolylineOptions polylineOptions = new PolylineOptions();
	            polylineOptions.add(latLng1, latlng2);

	            polylines.add(googleMap.addPolyline(polylineOptions));
            }

	        // Calculate acreage in enclosed loop
	        if (storedLocations.size() >= MIN_POINTS_FOR_LOOP)
	        {
		        String area = String.format("%.2f", calculateArea());

		        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		        String now = simpleDateFormat.format(new Date());

		        AlertDialog.Builder builder = new AlertDialog.Builder(this);

		        builder.setTitle("Fire Acreage");
		        builder.setMessage(area + "  acres, " + now);
		        builder.setPositiveButton("OK", null);

		        builder.show();

		        String fileEntry = area + ", " + now;
				NFSAreaFileManager.writeNewAreaToFile(fileEntry);
	        }
	        else
	        {
		        AlertDialog.Builder builder = new AlertDialog.Builder(this);

				builder.setTitle("Area too Small");
				builder.setMessage("There were less than 3 points found to calculate the area.");
				builder.setPositiveButton("OK", null);

				builder.show();
	        }

	        storedLocations = new ArrayList <Location> ();

	        buttonStart.setText("Start");

	        buttonPause.setEnabled(false);
	        buttonPause.setTextColor(Color.LTGRAY);

	        paused = false;
	        plotting = false;
        }
    }

    private void pauseButtonPressed()
    {
	    paused = !paused;

	    if (paused)
	    {
		    stopLocationUpdates();
		    buttonPause.setText("Resume");
	    }
	    else
	    {
		    startLocationUpdates();
		    buttonPause.setText("Pause");
	    }
    }

    private void viewRecentsButtonPressed()
    {
		String message = prepareRecentAreas();

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Recent Plots (Acres, Time)");
		builder.setMessage(message);
		builder.setPositiveButton("OK", null);

		builder.show();
    }

	private double calculateArea()
	{
		double area = 0;

		if (storedLocations.size() > MIN_POINTS_FOR_LOOP)
		{
			Location tempLocation1;
			Location tempLocation2;

			double sum1 = 0;
			double sum2 = 0;

			for (int i = 0; i < storedLocations.size() - 1; i++)
			{
				tempLocation1 = storedLocations.get(i);
				tempLocation2 = storedLocations.get(i + 1);

				sum1 = sum1 + Math.abs(tempLocation1.getLongitude()) * tempLocation2.getLatitude();
				sum2 = sum2 + tempLocation1.getLatitude() * Math.abs(tempLocation2.getLongitude());
			}

			tempLocation1 = storedLocations.get(storedLocations.size() - 1);
			tempLocation2 = storedLocations.get(0);

			sum1 = sum1 + Math.abs(tempLocation1.getLongitude()) * tempLocation2.getLatitude();
			sum2 = sum2 + tempLocation1.getLatitude() * Math.abs(tempLocation2.getLongitude());

			area = (sum1 - sum2) / 2;
			area = area * ACRES_PER_SQUARE_DEGREE;
		}

		return Math.abs(area);
	}

	private String prepareRecentAreas()
	{
		ArrayList <String> areasFromFile = NFSAreaFileManager.readAreaFile();

		String areaString = new String();

		for (String areaRecord : areasFromFile)
		{
			areaString = areaString + areaRecord + "\n";
		}

		return areaString;
	}

    private void createLocationRequest()
    {
        locationRequest = new LocationRequest();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    protected synchronized void buildGoogleApiClient()
    {
        apiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        apiClient.connect();
    }

    @Override
    public void onConnected(Bundle connectionHint)
    {
        startLocationUpdates();

		Location currentLocation = LocationServices.FusedLocationApi.getLastLocation(apiClient);

		this.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), 12.0f));
    }

    private void startLocationUpdates()
    {
        LocationServices.FusedLocationApi.requestLocationUpdates(apiClient, locationRequest, this);
    }

    private void stopLocationUpdates()
    {
        LocationServices.FusedLocationApi.removeLocationUpdates(apiClient, this);
    }

    @Override
    public void onConnectionSuspended(int cause)
    {
        Log.d("PlotActivity", "Cause: " + cause);
    }

    @Override
    public void onConnectionFailed(ConnectionResult result)
    {
        Log.d("PlotActivity", "Connection Failed: " + result);
    }

    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        this.googleMap = googleMap;
        this.googleMap.setMyLocationEnabled(true);
    }

    @Override
    public void onDestroy()
    {
        mapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory()
    {
        mapView.onLowMemory();
        super.onLowMemory();
    }

    @Override
    public void onPause()
    {
        mapView.onPause();
        super.onPause();

        stopLocationUpdates();

		if (plotting)
		{
			pauseButtonPressed();
		}
    }

    @Override
    public void onResume()
    {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        mapView.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onLocationChanged(Location location)
    {
		if (plotting)
		{
			if (location != null)
			{
				if (storedLocations.size() == 0)
				{
					storedLocations.add(location);
				} else if (storedLocations.size() > 0)
				{
					Location lastPoint = storedLocations.get(storedLocations.size() - 1);

					if (lastPoint.distanceTo(location) < MIN_DISTANCE)
					{
						return;
					}

					LatLng latLng1 = new LatLng(location.getLatitude(), location.getLongitude());
					LatLng latlng2 = new LatLng(lastPoint.getLatitude(), lastPoint.getLongitude());

					// draw line on map
					PolylineOptions polylineOptions = new PolylineOptions();
					polylineOptions.add(latLng1, latlng2);

					polylines.add(googleMap.addPolyline(polylineOptions));

					storedLocations.add(location);
				}
			}
		}
    }
}
